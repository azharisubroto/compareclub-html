$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function() {
        var benefitSliderChange = false;

        $('input[name="search[your_age]"], input[name="search[partner_age]"]').change(function(){
                var age     = rebateCalculator.convert_age($('input[name="search[your_age]"]:checked').val());
                var income  = rebateCalculator.convert_income($('#private_health_rebate').val());
                var result  = rebateCalculator.calculate(age, income);

                $('#rebate-tier').val(result.tier);
                $('#rebate-percentage').val(result.percentage/100);

                $('.the-rebate span').html('Tier ' + result.tier + ' ' + result.percentage + '%');

                quote.refine(SEARCH_BY_EXCESS);
        });

	// sidebar sliders
	function renderSlider(id, val, min, max, step) {
		var elem = $(id),
			hiddenfield = $(id).parent('.slider-wrap').find('input'),
			sliderOptions = $(id).parent('.slider-wrap').find('.slider-step-labels a');


		elem.slider({
		  range: "min",
		  value: val,
		  min: min,
		  max: max,
		  step: step,
		  slide: function (event, ui) {
			hiddenfield.val(sidebarSliderVal(ui.value));
			sidebarSliderLabel(sliderOptions, ui.value);

                        if (id === '#private_health_rebate_slider') {
                            // specific for income slider

                            var age     = rebateCalculator.convert_age($('input[name="search[your_age]"]:checked').val());
                            var income  = rebateCalculator.convert_income(hiddenfield.val());
                            var result  = rebateCalculator.calculate(age, income);

                            $('#rebate-tier').val(result.tier);
                            $('#rebate-percentage').val(result.percentage/100);

                            $('.the-rebate span').html('Tier ' + result.tier + ' ' + result.percentage + '%');

                            quote.refine(SEARCH_BY_EXCESS);
                        } else if (id === '#co_payment_slider' || id === '#excess_slider') {

                            if (id === '#excess_slider') {
                                if ($('#excess').val() == 'nil') {
                                    $('.co_payment_section').show();
                                } else {
                                    $('#co_payment').val('nil');
                                    $("#co_payment_slider").parent('.slider-wrap').find(".slider-step-labels a.active").removeClass('active');
                                    $("#co_payment_slider").parent('.slider-wrap').find(".slider-step-labels a[data-value='0']").addClass('active');
                                    $("#co_payment_slider").slider('value', 0).change();

                                    $('.co_payment_section').hide();
                                }
                            }

                            quote.refine(SEARCH_BY_EXCESS);
                        }

                        if (id === '#hospital-adjust' || id === '#extra-adjust') {

                            if (benefitSliderChange === false) {

                                benefitSliderChange = true;

                                swal({
                                    title: "Adjust Settings",
                                    text: "Using this feature will override your original selection. You can revert to your original selection by pressing the RESET button",
                                    icon: "info",
                                    button: "OK"
                                })
                                .then(function(result) {
                                    quote.refineByBenefitLevels();
                                });
                            } else {
                                quote.refineByBenefitLevels();
                            }
                        }
                    }
		});
		hiddenfield.val(sidebarSliderVal(val));

		sliderOptions.click(function(e){
			e.preventDefault();
			sidebarSliderLabel(sliderOptions, parseInt($(this).attr('data-value')));
			elem.slider('value', parseInt($(this).attr('data-value')));
		});

		sidebarSliderLabel(sliderOptions, val);

	}

	function sidebarSliderLabel(sliderOptions, val){
		sliderOptions.removeClass('active');
		sliderOptions.each(function(){
			if(parseInt($(this).attr('data-value')) === val) {
				$(this).addClass('active');
				return false
			}
		});
	}

	function sidebarSliderVal(val) {
		var label = 'nil';
		switch (val) {
			case 33:
				label = 'basic';
				break;
			case 66:
				label = 'medium';
				break;
			case 99:
				label = 'top';
				break;
			default:
				label = 'nil';
		}
		return label
	}

	$('.render-slider').each(function(){
		var id = '#' + $(this).attr('id'),
			val = parseInt($(this).attr('data-default')),
			min = parseInt($(this).attr('data-min')),
			max = parseInt($(this).attr('data-max')),
			step = $(this).attr('data-step') !== undefined ? parseInt($(this).attr('data-step')) : 33;

		if($(this).parent('.slider-wrap').find('.slider-step-labels a').length < 1)
			for(var x=step; x<=10;x++)
				$(id).parent('.slider-wrap').find('.slider-step-labels').append('<a data-value="'+x+'">'+x+'</a>');

		if(id === '#private_health_rebate_slider') {
			if(quote.settings.lifestage !== 'Single') {
				$(id).parent('.slider-wrap').find('.slider-step-labels a').remove();
				$(id).parent('.slider-wrap').find('.slider-step-labels').append('<a data-value="0">&lt;$180,000</a><a data-value="33" class="active">$180,001-210,000</a><a data-value="66">$210,001-280,000</a><a data-value="99">&gt;$280,000</a>')
			}
			if(quote.settings.lifestage === 'Single Parent Family') {
				$('.show-when-single-parent').show();
			} else if(quote.settings.lifestage === 'Couple') {
				$('.health_rebate_label').text('Income (couple)');
				$('.show-when-couple').show();
			} else if(quote.settings.lifestage === 'Family') {
				$('.health_rebate_label').text('Income (You and spouse)');
				$('.show-when-family').show();
			}
		}

		renderSlider(id, val, min, max, step);

	});

	// result panel
	$('.toggleDetails').click(function(e){

		if($(this).attr('href') != '#') {
			return
		} else {
			e.preventDefault();
		}

				$('.policiesWrapper .toggleDetails').toggleClass('active');

		// toggle hidden content
		$(this).parents('.policiesWrapper').find('.toggableContent').slideToggle();

		if(!$(this).hasClass('active')) {
			$('.see-more-btn span:not(.btn-icon)').text('See More');
			$('.see-more-btn').removeClass('active');
			$('.see-more-content').hide();
		}

		/*$(this).toggleClass('active');

		// toggle compare display
		if($('.toggleDetails.active').length == 2) {
			$(this).parents('.policiesWrapper').addClass('flexi')
		} else {
			$(this).parents('.policiesWrapper').removeClass('flexi');
		}
		// toggle hidden content
		$(this).parents('.product-entry').find('.toggableContent').slideToggle();

		if(!$(this).hasClass('active')) {
			$('.see-more-btn span:not(.btn-icon)').text('See More');
			$('.see-more-btn').removeClass('active');
			$('.see-more-content').hide();
		}*/
	});

	$('.see-more-btn').click(function(e){
		e.preventDefault();

		var btnText = ($(this).find('span:not(.btn-icon)').text() === 'See More' ? 'Show Less' : 'See More');
		var coverSectionClass = '.' + $(this).parents('.cover-section').attr('class').split(' ').join('.');
		var top = $(coverSectionClass).offset().top;

		$(coverSectionClass).find('.see-more-content').slideToggle();
		$(coverSectionClass).find('.see-more-btn span:not(.btn-icon)').text(btnText);
		$(coverSectionClass).find('.see-more-btn').toggleClass('active');

		$("html, body").animate({
		  scrollTop: top
		}, 600, "swing");

	});

	// enable tooltip
	$('[data-toggle="tooltip"]').tooltip({html:true});
	$('.info-tip').click(function(e){
		e.preventDefault();
	});


	// filter on mobile

	var windowScreen = $(window).width();
	function getWindowScreen() {
		windowScreen = $(window).width();
	}
	$( window ).resize(function(){getWindowScreen()});

	var filterText = (windowScreen < 1100 ? 'Filters' : 'Refine Results');
	$('.filter-nav-text').text(filterText);

	$('.filter-nav').click(function(e){
		e.preventDefault()
		var filterText = $('.filter-nav-text').text();
		var navText = (filterText === 'Filters' ? 'Refine Results' : 'Filters');
		if(filterText === 'Filters') {
			$('.toggleFilter').slideDown();
			$('.filter-backdrop').fadeIn();
		} else {
			$('.toggleFilter').slideUp();
			$('.filter-backdrop').fadeOut();
		}
		$('.filter-nav-text').text(navText)
	});


	// content slider
	var windowscreen = $( window ).width();

	function responsiveSlider(element, slickSettings){
		var defaultUnslick = { breakpoint: windowscreen * 10, settings: "unslick"}
		var slickSettings = {
			responsive: slickSettings
		};
		slickSettings.responsive.unshift(defaultUnslick);
		if(!$(element).hasClass('slick-initialized')) {
			$(element).slick(slickSettings);
		}
	}

	var sliderSettings = [
		{
			selector : '.contentSlider1',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 3,
						dots: false,
						arrows: true,
						infinite: false
					}
				},
				{
					breakpoint : 767,
					settings: {
						slidesToShow: 1,
						infinite: false
					}
				}
			]
		},
		{
			selector : '.contentSlider2',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 4,
						dots: false,
						arrows: true,
						infinite: false
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToScroll: 2,
						slidesToShow: 2,
						infinite: false
					}
				}
			]
		},
		{
			selector : '.contentSlider3',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 1,
						dots: false,
						arrows: true,
						infinite: false
					}
				},
				{
					breakpoint: 767,
					settings: {
						dots: true,
						arrows: true,
						infinite: false,
						adaptiveHeight: true
					}
				}
			]
		},
		{
			selector : '.contentSlider4',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 1,
						dots: false,
						arrows: true,
						infinite: false
					}
				},
				{
					breakpoint: 767,
					settings: "unslick"
				}
			]
		},
		{
			selector : '.life-stages-grp',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 1,
						dots: true,
						arrows: false,
						infinite: false,
						adaptiveHeight: true
					}
				}
			]
		},
		{
			selector : '#testimonials',
			settings : [
				{
					breakpoint: 1000,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 1,
						dots: true,
						arrows: false,
						infinite: true,
						adaptiveHeight: true
					}
				}
			]
		},
		{
			selector : '.contentSlider5',
			settings : [
				{
					breakpoint: 767,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 1,
						dots: true,
						arrows: false,
						infinite: true,
						adaptiveHeight: true
					}
				}
			]
		},
		{
			selector : '.feed-section .col-80',
			settings : [
				{
					breakpoint: 767,
					settings: {
						slidesToScroll: 2,
						slidesToShow: 2,
						dots: true,
						arrows: true,
						infinite: true
					}
				}
			]
		}
	];

	var generateSliders = function(sliderSettings){
		var sliderSettings = sliderSettings;
		sliderSettings.forEach(function(e, i){
			responsiveSlider(e.selector, e.settings)
		})
	};

	generateSliders(sliderSettings);

	$( window ).resize(function() {
		windowscreen = $( window ).width();
		generateSliders(sliderSettings);
	});


	// mega menu
	$('#toggleMegamenu').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parents('.mega-menu').find('.main-menu').slideToggle();
	});

	// leadform validation
	$('.leadform button').click(function(e){
		e.preventDefault();
		var stageError = '<strong class="error stage-error show">Please select your stage</strong>',
			stateError = '<strong class="error state-error show">Please select your state</strong>';

		if($('select[name="stage"]').val() == "Stage" || $('select[name="state"]').val() == "State") {
			if($('select[name="stage"]').val() == "Stage") {
				$('select[name="stage"]').parents('.col-sm-6').append(stageError)
			}
			if($('select[name="state"]').val() == "State") {
				$('select[name="state"]').parents('.col-sm-6').append(stateError)
			}
			return false;
		} else {
			$('.leadform').submit();
		}

	});

	$('.leadform select').change(function(){
		$(this).parents('.col-sm-6').find('.error').removeClass('show').remove();
	});

	// tier table
	var couple_th = "<th>Age/income</th><th><$180,000</th><th>$180,001 - $210,000</th><th>$210,001 - $280,000</th><th>>$280,000</th>";
	var single_th = "<th>Age/income</th><th><$90,000</th><th>$90,001 - $105,000</th><th>$105,001 - $140,000</th><th>>$140,000</th>";


	$('.the-rebate a').click(function(){
            $('.modal_life_stage').text(quote.settings.lifestage);
            if(quote.settings.lifestage !== 'Single') {
                $('.modal_age_income').html(couple_th);
            } else {
                $('.modal_age_income').html(single_th);
            }
	});

	$('#rebatemodal tbody td:not(:first-child)').click(function() {
		$('#rebatemodal tbody td:not(:first-child)').removeClass('selected');
		$(this).addClass('selected');

		var percent = $(this).attr('data-value');
		var tier    = $(this).attr('data-tier');
                var age     = $(this).attr('data-age');

		$('.modal-footer button').attr('data-percent', percent);
		$('.modal-footer button').attr('data-tier', tier);
                $('.modal-footer button').attr('data-age', age);
	});

	$('.modal-footer button').on('click',function() {
		var percent = $(this).attr('data-percent');
		var tier    = $(this).attr('data-tier');
                var age     = $(this).attr('data-age');
		$('#your_age_65').removeAttr('checked');
                $('#your_age_69').removeAttr('checked');
                $('#your_age_70').removeAttr('checked');
                $('#partner_age_65').removeAttr('checked');
                $('#partner_age_69').removeAttr('checked');
                $('#partner_age_70').removeAttr('checked');

		if(percent){
                    var percentage = parseFloat(percent.replace('%', '')/100);
                    $('input[name="rebate-precent"]').val(percentage);
                    $('input[name="rebate-tier"]').val(tier);
                    $('.rebate-precent').text(percent);
                    $('.rebate-tier').text('Tier: '+tier);
                    $('#rebate-percentage').val(percentage);
                    $('#rebate-tier').val(tier);

                    $('#your_age_'+age).attr('checked', true);
                    $('#partner_age_'+age).attr('checked', true);
		}

		if($('.the-rebate').length > 0) {
			$('.the-rebate span').text('Tier '+tier+' '+percent);

			var rebate_slide_value = 99;
			switch (parseInt(tier)) {
				case 0:
					rebate_slide_value = 0;
					break;
				case 1:
					rebate_slide_value = 33;
					break;
				case 2:
					rebate_slide_value = 66;
					break;
				default:
					rebate_slide_value = 99;
			}

			$('#private_health_rebate_slider').slider("option", "value", rebate_slide_value);

			var hiddenfield = $('#private_health_rebate_slider').parent('.slider-wrap').find('input'),
                        sliderOptions = $('#private_health_rebate_slider').parent('.slider-wrap').find('.slider-step-labels a');

			hiddenfield.val(sidebarSliderVal(rebate_slide_value));
			sidebarSliderLabel(sliderOptions, rebate_slide_value);
		}

		$('#rebatemodal').modal('hide');

                quote.refine(SEARCH_BY_EXCESS);
	});

	// extras options
	function submitExtras(section) {

		// set variables
		var form = section.find('form'),
			formData = form.serializeObject(),
			checkbox = form.find('input'),
			checkboxChecked = form.find('input:checked'),
			currentName = [];

		// error classes
		section.removeClass('haserror');
		function showError() {
			var sectionScroll = $('.text-error').length > 0 ? $('.text-error').eq(0) : section;
			$('html, body').animate({
				scrollTop: sectionScroll.offset().top - 50
			}, 100);
			section.addClass('haserror');
		}

		// get checkbox names to separate sections
		checkbox.each(function(){
			var checkName = $(this).attr('name');
			if(currentName.indexOf(checkName) == -1)
				currentName.push(checkName);
		});

		// validate form if user didn't select option
		currentName.forEach(function(item){
			if($('input[name='+item+']:checked').length == 0)
				showError()
				return false;
		});

		// check if the selected option requires to fill the textarea
		checkboxChecked.each(function(){
			var elem = $(this),
				wrap = elem.parents('.options-wrap');

			wrap.removeClass('text-error')
			if(elem.hasClass('trigger-textarea'))
			 	if(wrap.find('textarea').val() === '') {
					wrap.addClass('text-error');
					showError();
					return false
				}

		});

		function submitForm(formData) {

			var apiUrl = getCookie('api_endpoint')+'/';

			$.ajax({
	            url: apiUrl + 'hic/customerlastexp',
	            dataType: 'json',
	            type: 'POST',
	            data: formData,
	            success: function(data) {
	              console.log(data);
				  $('.extra-questions .panel-heading').text('Got it!');
				  $('.extra-questions .panel-body').html('<p class="text-center">We\'ll help you address those concerns very soon.</p>');
	            },
	            error: function(e) {
	               console.log("there'\s an error submitting the lead", e);
	            }
	        });

		}

		if(!section.hasClass('haserror')) {

			formData.frustration_reason = typeof formData.frustration_reason !== 'string' ? formData.frustration_reason.join(', ') : formData.frustration_reason;
			formData.frustration_worry_about = typeof formData.frustration_worry_about !== 'string' ? formData.frustration_worry_about.join(', ') : formData.frustration_worry_about;

			// data specific
			if(formData.other_details !== '' && formData.frustration_reason.indexOf('Other') !== -1)
				formData.frustration_reason += ', ' + formData.other_details;

			if(formData.other_details_worry !== '' && formData.frustration_worry_about.indexOf('Other') !== -1)
				formData.frustration_worry_about += ', ' + formData.other_details_worry;

			delete formData.other_details;
			delete formData.other_details_worry;

			formData.apikey = API_KEY;
			formData.uuid = LEAD_UUID;

			submitForm(formData);

		}


	}

	$('.extra-questions button').click(function(e) {
		e.preventDefault();
		var section = $(this).parents('.extra-questions');
		submitExtras(section);
	});

	$('.trigger-textarea').change(function(){
		var textArea = $(this).parents('.options-wrap').find('textarea');
		if($(this).is(':checked')) {
			textArea.removeAttr('disabled').addClass('has-content');
			textArea.focus();
		} else {
			textArea.attr('disabled', 'disabled').val('').removeClass('has-content');
		}

	});

})
